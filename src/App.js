import React, { Component } from 'react';
import './App.css';
import Person from './App/Component/Person'



class App extends Component {
  
  state = {
    persons: [
      {id:'adaa',name:'riii',age:19},
      {id:'adwad',name: 'brian',age : 16},
      {id:'ad111',name: 'angga',age : 17},
    ],
    showPerson : false,
  }
  hideIt = () =>{
    let doesShow = this.state.showPerson;
    this.setState({showPerson:!doesShow})
  }
//Test
  nameChange = (event,id) =>{
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });
    const person = {
      ...this.state.persons[personIndex]
    };
    
    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState({persons : persons})
  }

  deletePerson = (personIndex) => {
      const persons = [...this.state.persons];
      persons.splice(personIndex,1);
      this.setState({persons: persons});
  }

  render() {
    const style = {
      color:'white',
      backgroundColor:'green',
      font:'inherit',
      border:'1px solid black',
      cursor: 'pointer',
      
    }
    let persons = null;

    if(this.state.showPerson){
      persons = (
        <div>
          {this.state.persons.map((person,index)  =>{
            return <Person
            changed={(event)=>this.nameChange(event,person.id)}
            name = {person.name}
            age = {person.age}
            key={person.id}
            click= {() => this.deletePerson(this.props.index)} />
          })}
       
       
      </div>);
      style.backgroundColor = 'red';
    }
    const classes = [];
    if(this.state.persons.length <= 2)
        classes.push('red');
    if(this.state.persons.length <=1)
        classes.push('bold');

    return (
      <div className="App">
      <h1>Hi </h1>
      <h3 className={classes.join(' ')}>Working</h3>
      <button
       style={style}
       onClick={this.hideIt}>Switch Up</button>
       {persons}
       
           
      </div>
    );
  }
  
}

export default App;
